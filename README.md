# README #

SPA para consumir a API pública de fatos/piadas do Chuck Norris (https://api.chucknorris.io/)

### How do I get set up? ###

Para instalar a aplicação é necessário ter instalado NodeJS e Npm
Após instalados execute o comando npm install
Para executar utilize npm start e acesse o link que irá aparecer no terminal, normalmente é "http://localhost:8080/"

Para ter a versão dos arquivos minificados para produção, basta executar o comando "npm run prod"