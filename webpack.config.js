var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');

var path = require("path");

module.exports = {
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: 'main.js',
      publicPath: '/'
    },
    module: {
      rules: [
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
              fallback: 'style-loader',
              use: ['css-loader','sass-loader'],
              publicPath: '/dist'
          })
        },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: 'babel-loader'
        }
      ]
    },
    devServer: {
        contentBase: path.join(__dirname, "dist"),
        compress: true,
        stats: "errors-only",
        open: false
    },
    plugins: [
      new HtmlWebpackPlugin({
          title: 'Chuck Norris Facts',
          minify: {
              collapseWhitespace: true
          },
          hash: false,
          template: './src/index.html',
      }),
      new ExtractTextPlugin({
          filename: 'main.css',
          disable: false,
          allChunks: true
      })
    ]
}
