import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory, IndexRoute } from "react-router";
import {Provider} from "react-redux";

const css = require('./sass/main.scss');

import Layout from "./components/layout";
import Categories from "./components/categories";
import Fact from "./components/fact";

import store from './store';

class App extends Component {
  render() {
    return (
      <Router history={hashHistory}>
          <Route path={"/"} component={Layout} >
              <IndexRoute component={Categories} />
              <Route path='categories' component={Categories} />
              <Route path={"/:id"} component={Fact} />
          </Route>
      </Router>
    );
  }
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
