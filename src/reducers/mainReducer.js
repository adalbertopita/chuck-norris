function chuck(state = {
  fact: [],
  categories : [],
  isLoading: false
}, action){

  switch(action.type) {

    case "FETCH_CATEGORIES" :
        return state = {
          ...state,
          categories : action.payload,
          isLoading: false
        };

    case "FETCH_FACT" :
        return state = {
          ...state, 
          fact: action.payload,
          isLoading: false
         };
    
    case "TRIGGER_LOADING":
        return state = {
          ...state,
          isLoading: action.payload
        };

    default:
        return state;
  }
}

export default chuck;
