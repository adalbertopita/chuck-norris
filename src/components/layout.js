import React, { Component } from 'react';
import LoadingBar from 'react-redux-loading-bar'

class Layout extends Component {
  render() {
    return (
    <div>
        <LoadingBar />
        <div className="wrapper">
          <div className="container">
              {this.props.children}
          </div>
        </div>
    </div>
    );
  }
}

export default Layout;
