import React, { Component } from 'react';
import {connect} from "react-redux";
import { getCategories } from "../actions/mainActions";

import CategoryItem from "../components/categoryItem";
import Loading from "../utilities/loading";

class Categories extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount(){
    this.props.getCategories();
  }

  render() {
    const { categories } = this.props;
    if(this.props.isLoading){
      return (
        <Loading />
      )
    }else{
      return (
        <ul className="categories-list mdl-list">
            {categories.map((cat, x) => <CategoryItem 
                                    key={x} 
                                    name={cat}
                                />
                )}
        </ul>
      );
    }
    
  }
}

const mapStateToProps = (state) => {
  return {
      categories: state.categories,
      isLoading: state.isLoading
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getCategories: () => {
            dispatch(getCategories());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
