import React from 'react';
import {Link} from "react-router";

const CategoryItem = (props) => {
    let cat_link = "#/" + props.name;
    return (
        <li className="mdl-list__item">
            <span className="mdl-list__item-primary-content">
                <a href={cat_link}>{props.name}</a>
            </span>
        </li>
    );
}

export default CategoryItem