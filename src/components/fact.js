import React, { Component } from 'react';
import {connect} from "react-redux";
import { hashHistory } from "react-router";

import { getFact } from "../actions/mainActions";

import Loading from "../utilities/loading";

class Fact extends Component {

  constructor(props) {
    super(props);
    this.getFact = this.getFact.bind(this);
    this.handleBackButton = this.handleBackButton.bind(this);
  }

  componentDidMount(){
    this.getFact();
  }

  getFact(){
    this.props.getFact(this.props.params.id);
  }

  handleBackButton(){
    hashHistory.push('/');
  }

  render() {
    const { fact, isLoading } = this.props;

    console.log(isLoading);

    if(isLoading){
      return (
        <Loading/>
      )
    }else{
      return (
        <div className="fact mdl-card mdl-shadow--2dp">
          <div className="mdl-card__title">
            <h2 className="mdl-card__title-text">{this.props.params.id}</h2>
          </div>
          <div className="mdl-card__supporting-text">
            {fact.value}
          </div>
          <div className="mdl-card__actions mdl-card--border">
            <a onClick={this.getFact} className="mdl-button mdl-button--colored mdl-js-button">
              another fact
            </a>

            <a onClick={this.handleBackButton} className="back-button mdl-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-button--mini-fab">
              <i className="material-icons">navigate_before</i>
            </a>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
      fact: state.fact,
      isLoading: state.isLoading
  };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getFact: (category) => {
            dispatch(getFact(category));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Fact);
