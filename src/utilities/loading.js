import React from 'react';

/*
  
*/

const Loading = (props) => {

  return(
    <div className="fact mdl-card mdl-shadow--2dp">
      <div className="loading">loading...</div>
    </div>
  )

}

export default Loading
