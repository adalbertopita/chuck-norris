import axios from "axios";

const API_URL = "https://api.chucknorris.io/jokes";

export function getCategories() {
  
  return function(dispatch){

    dispatch({
      type: "TRIGGER_LOADING",
      payload: true
    });

    axios.get(API_URL+"/categories")
      .then((response)=>{

        dispatch({
          type: "FETCH_CATEGORIES",
          payload: response.data
        });
        
      })
      .catch((err)=> {
          console.log('erro: ', err)
      })

  };
}


export function getFact(category){

  return function(dispatch){

    dispatch({
      type: "TRIGGER_LOADING",
      payload: true
    });

    axios.get(API_URL+"/random?category="+category)
      .then((response)=>{

        dispatch({
          type: "FETCH_FACT",
          payload: response.data
        });
        
      })
      .catch((err)=> {
          console.log('erro: ', err)
      })
  }

}