import {createStore, applyMiddleware} from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";

import mainReducer from "./reducers/mainReducer";

const store = createStore(mainReducer,
      applyMiddleware(
        promise(),
        //logger(),
        thunk
      )
);

export default store;